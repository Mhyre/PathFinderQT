#include "persomodel.h"

PersoModel::PersoModel(QObject *parent){}

int PersoModel::rowCount(const QModelIndex &parent) const{
    return m_listPersonnages.count();
}

QHash<int, QByteArray> PersoModel::roleNames() const{
    QHash<int, QByteArray> roles;
    roles[AgeRole] = "age";
    roles[SexeRole] = "sexe";
    roles[PrenomRole] = "prenom";
    roles[NomRole] = "nom";
    roles[PersonnageRole] = "perso";
    /*
    roles[ClasseRole] = "classe";
    roles[RaceRole] = "race";
    roles[PersonnageRole] = "personnage";
    */
    return roles;
}

bool PersoModel::isIndexValid(const int &index) const{
    return index >= 0 || index < m_listPersonnages.count();
}

QVariant PersoModel::data(const QModelIndex &index, int role) const{
    if (!isIndexValid(index.row()))
        return QVariant();

    Personnage *perso = m_listPersonnages[index.row()];
    switch (role) {

    case AgeRole:
        return perso->age();

    case SexeRole:
        return perso->sexe();

    case PrenomRole:
        return perso->prenom();

    case NomRole:
        return perso->nom();

    case PersonnageRole:
        return QVariant::fromValue(perso);
/*
    case ClasseRole:
        return perso->classe();

    case RaceRole:
        return perso->race();*/

    default:
        return QVariant();
    }
}

bool PersoModel::setData(const QModelIndex &index, const QVariant &value, int role){
    if (!isIndexValid(index.row())){
        return false;
    }

    if (data(index, role) == value){
        return true;
    }

    Personnage *perso = m_listPersonnages[index.row()];
    switch (role) {

    case NomRole:
        perso->setNom(value.toString());

    case PrenomRole:
        perso->setPrenom(value.toString());

    case AgeRole:
        perso->setAge(value.toInt());

    case SexeRole:
        perso->setSexe(value.toInt());
/*
    case ClasseRole:
        return perso->setClasse(value);

    case RaceRole:
        return perso->setRace(value);*/
    }

    QVector<int> roles;
    roles.append(role);

    QModelIndex topLeft = index.sibling(0,0);
    QModelIndex bottomRight = index.sibling(m_listPersonnages.count()-1,0);

    emit dataChanged(topLeft, bottomRight, roles);

    return true;
}

void PersoModel::addPersonnage(Personnage *perso){
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_listPersonnages << perso;
    endInsertRows();
}

void PersoModel::addGenericPersonnage(){
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_listPersonnages << new Personnage("nouveau","perso",0,20,new Classe("","",""),new Race("","",""));
    endInsertRows();
}


bool PersoModel::removeRow(int row, const QModelIndex &parent){
    if (!isIndexValid(row)){
        return false;
    }

    beginRemoveRows(parent, row, row+1);

    m_listPersonnages.removeAt(row);

    endRemoveRows();
    return true;
}
