#ifndef PERSOMODEL_H
#define PERSOMODEL_H

#include <QAbstractListModel>

#include "personnage.h"

class PersoModel: public QAbstractListModel
{
    Q_OBJECT
public:
    enum PersoRole{
        DesignationRole = Qt::UserRole,
        AgeRole,
        SexeRole,
        PrenomRole,
        NomRole,
        PersonnageRole
        /*
        ClasseRole,
        RaceRole
        */
    };

    explicit PersoModel(QObject *parent = nullptr);


    QString     groupName() const;

    void        addPersonnage(Personnage *personnage);
    int         rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant    data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    bool        setData(const QModelIndex &index, const QVariant &value, int role);

    bool isIndexValid(const int &index) const;

    Q_INVOKABLE void addGenericPersonnage();
    Q_INVOKABLE bool removeRow(int row, const QModelIndex &parent = QModelIndex());
signals:

public slots:

protected:
    QHash<int, QByteArray> roleNames() const;

private:
    QList<Personnage*> m_listPersonnages;


};

#endif // PERSOMODEL_H
