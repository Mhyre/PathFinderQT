#include "race.h"


Race::Race(QString nom, QString description, QString pathImage)
    :m_nom(nom),m_description(description),m_pathImage(pathImage)
{
}

QString Race::nom() const
{
    return m_nom;
}

QString Race::description() const
{
    return m_description;
}

QString Race::pathImage() const
{
    return m_pathImage;
}

void Race::setNom(QString arg)
{
    if (m_nom == arg)
        return;

    m_nom = arg;
    emit nomChanged(arg);
}

void Race::setDescription(QString arg)
{
    if (m_description == arg)
        return;

    m_description = arg;
    emit descriptionChanged(arg);
}

void Race::setPathImage(QString arg)
{
    if (m_pathImage == arg)
        return;

    m_pathImage = arg;
    emit pathImageChanged(arg);
}

void Race::addStat(Statistiques stat, int valeur){
    stats[stat] = valeur;
}
