#ifndef UTILS_H
#define UTILS_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <list>

const int NOMBRE_LANCER = 3;

int nombreAleatoire()
{
    srand(time(NULL));
    return rand()%6+1;
}

bool compFirst(const int & a, const int & b) { return a < b; }

list<int> lancementDes()
{
    list<int> nombres_generes;
    for(int i = 0; i < NOMBRE_LANCER; ++i)
    {
        nombres_generes.push_back(nombreAleatoire());
    }
    nombres_generes.sort(compFirst);

    return nombres_generes;
}

#endif // UTILS_H
