#ifndef CQNPVUC_H
#define CQNPVUC_H

#include <iostream>
#include <map>

using namespace std;

enum Statistiques {
    Force, Dexterite, Constitution, Intelligence, Sagesse, Charisme
};

#endif // CQNPVUC_H
