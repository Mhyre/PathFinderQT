#ifndef DBMANAGER_H
#define DBMANAGER_H

#include <QString>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include "personnage.h"

class DbManager
{
public:
    DbManager();
    bool addPersonnage(const Personnage*&);
    bool removePersonnage(const Personnage*&);
    list<Personnage*> getPersonnages();
private:
    QSqlDatabase m_db;

    QString DATABASENAME = "PathFinderdb";
};

#endif // DBMANAGER_H
