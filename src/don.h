#ifndef DON_H
#define DON_H

#include "CQNPVUC.h"

#include <QString>
#include <QHash>
#include <QObject>

class Don : public QObject
{
    Q_OBJECT
private:
    Q_PROPERTY(QString nom READ nom WRITE setnom NOTIFY nomChanged)
    Q_PROPERTY(QString description READ description WRITE setDescription NOTIFY descriptionChanged)
    QHash<Statistiques, int> stats;

    void addStat(Statistiques stat, int valeur);

    QString m_nom;

    QString m_description;

public:
    Don(QString nom, QString description);

    friend bool operator==(const Don & d1, const Don & d2);
    QString nom() const;
    QString description() const;

public slots:
    void setnom(QString arg);
    void setDescription(QString arg);

signals:
    void nomChanged(QString arg);
    void descriptionChanged(QString arg);
};


bool operator==(const Don & d1, const Don & d2);

#endif // DON_H
