#include "classe.h"


Classe::Classe(QString nom, QString description, QString pathImage)
    :m_nom(nom),m_description(description),m_pathImage(pathImage)
{

}

QString Classe::nom() const
{
    return m_nom;
}

QString Classe::description() const
{
    return m_description;
}

QString Classe::pathImage() const
{
    return m_pathImage;
}

void Classe::setNom(QString arg)
{
    if (m_nom == arg)
        return;

    m_nom = arg;
    emit nomChanged(arg);
}


void Classe::setDescription(QString arg)
{
    if (m_description == arg)
        return;

    m_description = arg;
    emit descriptionChanged(arg);
}

void Classe::setPathImage(QString arg)
{
    if (m_pathImage == arg)
        return;

    m_pathImage = arg;
    emit pathImageChanged(arg);
}

