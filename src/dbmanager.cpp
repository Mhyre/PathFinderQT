#include "dbmanager.h"
#include <QDebug>
#include "personnage.h"

DbManager::DbManager()
{
   m_db = QSqlDatabase::addDatabase("QSQLITE");
   m_db.setDatabaseName(DATABASENAME);

   if (!m_db.open())
   {
      qDebug() << "Error: connection with database fail";
   }
   else
   {
      qDebug() << "Database: connection ok";
   }
   QSqlQuery query(m_db);
   query.exec("create table if not exists personnage "
             "(id integer primary key autoincrement, "
             "nom varchar(150))");

}

bool DbManager::addPersonnage(const Personnage*& perso)
{
   bool success = false;
   // you should check if args are ok first...
   QSqlQuery query(m_db);

   query.prepare("INSERT INTO personnage(nom) VALUES(:nom)");
   query.bindValue(":nom", perso->nom());

   if(query.exec())
   {
       success = true;
   }
   else
   {
        qDebug() << "addPerson error:  "
                 << query.lastError();
   }

   return success;
}

bool DbManager::removePersonnage(const Personnage *& perso)
{
    bool success = false;
    QSqlQuery query(m_db);
    list<Personnage*> liste;
    query.prepare("DELETE FROM personnage WHERE nom = '" + perso->nom() + "'");

    if(query.exec())
    {
        qDebug() << "J'ai bien supprimé le personnage:  "
                 << query.lastError();
        success = true;
    }
    else
    {
         qDebug() << "Une erreur est survenue. "
                  << query.lastError();
    }

    return success;

}

list<Personnage*> DbManager::getPersonnages(){
    QSqlQuery query(m_db);
    list<Personnage*> liste;

    query.prepare("SELECT nom FROM personnage;");

    if(query.exec())
    {
        while (query.next()) {
            QString country = query.value(0).toString();
            liste.push_back(new Personnage(country,"",0,0,new Classe("","",""), new Race("","","")));
            qDebug() << "super ça a marché : "
                     << country;
        }
    }
    else
    {
         qDebug() << "addPerson error:  "
                  << query.lastError();
    }
    return liste;
}
