#include "sort.h"

bool operator==(const Sort &s1, const Sort &s2)
{
    return s1.m_name == s2.m_name;
}

Sort::Sort(QString nom, QString description)
    : m_name(nom), m_description(description)
{}


void Sort::setName(QString arg)
{
    if (m_name == arg)
        return;

    m_name = arg;
    emit nameChanged(arg);
}

void Sort::setDescription(QString arg)
{
    if (m_description == arg)
        return;

    m_description = arg;
    emit descriptionChanged(arg);
}

QString Sort::name() const
{
    return m_name;
}

QString Sort::description() const
{
    return m_description;
}



