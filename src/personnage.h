#ifndef PERSONNAGE_H
#define PERSONNAGE_H

#include <QString>
#include <QObject>
#include <list>

#include "race.h"
#include "classe.h"
#include "sort.h"
#include "don.h"


using namespace std;

class Personnage : public QObject
{
    Q_OBJECT
private :
    Q_PROPERTY(QString nom READ nom WRITE setNom NOTIFY nomChanged)
    Q_PROPERTY(QString prenom READ prenom WRITE setPrenom NOTIFY prenomChanged)
    Q_PROPERTY(qint8 sexe READ sexe WRITE setSexe NOTIFY sexeChanged)
    Q_PROPERTY(qint8 age READ age WRITE setAge NOTIFY ageChanged)
    Q_PROPERTY(Classe* classe READ classe WRITE setClasse NOTIFY classeChanged)
    Q_PROPERTY(Race* race READ race WRITE setRace NOTIFY raceChanged)

    list<Don*> dons;
    list<Sort*> sorts;

    QString m_nom;
    QString m_prenom;
    qint8 m_sexe;
    qint8 m_age;
    Classe* m_classe;
    Race* m_race;

public:
    Personnage(QString nom, QString prenom, int sexe, int age, Classe* classe, Race* race);
    QString getNom();
    QString getPrenom();
    const list<Don*> getDons() const;
    const list<Sort*> getSorts() const;
    bool addDon(Don* don);
    bool addSort(Sort* sort);
    bool removeDon(Don* don);
    bool removeSort(Sort* sort);

    qint8 age() const;
    qint8 sexe() const;
    QString prenom() const;
    QString nom() const;
    Classe* classe() const;
    Race* race() const;

public slots:
    void setAge(qint8 arg);
    void setSexe(qint8 arg);
    void setPrenom(QString arg);
    void setNom(QString arg);
    void setClasse(Classe* arg);
    void setRace(Race* arg);

signals:
    void ageChanged(qint8 arg);
    void sexeChanged(qint8 arg);
    void prenomChanged(QString arg);
    void nomChanged(QString arg);
    void classeChanged(Classe* arg);
    void raceChanged(Race* arg);
};

#endif // PERSONNAGE_H
