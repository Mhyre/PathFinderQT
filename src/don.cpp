#include "don.h"


Don::Don(QString nom, QString description)
    :m_nom(nom), m_description(description)
{
}

QString Don::nom() const
{
    return m_nom;
}

QString Don::description() const
{
    return m_description;
}

void Don::setnom(QString arg)
{
    if (m_nom == arg)
        return;

    m_nom = arg;
    emit nomChanged(arg);
}

void Don::setDescription(QString arg)
{
    if (m_description == arg)
        return;

    m_description = arg;
    emit descriptionChanged(arg);
}


bool operator==(const Don &d1, const Don &d2)
{
    return d1.nom() == d2.nom();
}
