#ifndef SORT_H
#define SORT_H

#include <QString>
#include <QObject>

#include "CQNPVUC.h"

class Sort  : public QObject
{
    Q_OBJECT
private:

    map<Statistiques, int> prerequis;

    void addPrerequis(Statistiques stat, int valeur);
    friend bool operator==(const Sort & s1, const Sort & s2);

    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString description READ description WRITE setDescription NOTIFY descriptionChanged)

    QString m_name;

    QString m_description;

public:
    Sort(QString nom, QString description);
    QString name() const;
    QString description() const;

public slots:
    void setName(QString arg);
    void setDescription(QString arg);

signals:
    void nameChanged(QString arg);
    void descriptionChanged(QString arg);
};

bool operator==(const Sort & s1, const Sort & s2);

#endif // SORT_H
