#include "personnage.h"


Personnage::Personnage(QString nom, QString prenom, int sexe, int age, Classe* classe, Race* race)
    :m_nom(nom), m_prenom(prenom), m_sexe(sexe), m_age(age), m_classe(classe), m_race(race)
{

}

const list<Don*> Personnage::getDons() const{
    return this->dons;
}

const list<Sort*> Personnage::getSorts() const{
    return this->sorts;
}

bool Personnage::addDon(Don* don){
    this->dons.push_back(don);
    return true;
}

bool Personnage::addSort(Sort* sort){
    this->sorts.push_back(sort);
    return true;
}

bool Personnage::removeDon(Don* don){
    this->dons.remove(don);
    return true;
}

bool Personnage::removeSort(Sort* sort){
    this->sorts.remove(sort);
    return true;
}

qint8 Personnage::age() const
{
    return m_age;
}

qint8 Personnage::sexe() const
{
    return m_sexe;
}

QString Personnage::prenom() const
{
    return m_prenom;
}

QString Personnage::nom() const
{
    return m_nom;
}

Classe* Personnage::classe() const
{
    return m_classe;
}

Race* Personnage::race() const
{
    return m_race;
}

void Personnage::setAge(qint8 arg)
{
    if (m_age == arg)
        return;

    m_age = arg;
    emit ageChanged(arg);
}

void Personnage::setSexe(qint8 arg)
{
    if (m_sexe == arg)
        return;

    m_sexe = arg;
    emit sexeChanged(arg);
}

void Personnage::setPrenom(QString arg)
{
    if (m_prenom == arg)
        return;

    m_prenom = arg;
    emit prenomChanged(arg);
}

void Personnage::setNom(QString arg)
{
    if (m_nom == arg)
        return;

    m_nom = arg;
    emit nomChanged(arg);
}

void Personnage::setClasse(Classe* arg)
{
    if (m_classe == arg)
        return;

    m_classe = arg;
    emit classeChanged(arg);
}

void Personnage::setRace(Race* arg)
{
    if (m_race == arg)
        return;

    m_race = arg;
    emit raceChanged(arg);
}
