#ifndef CLASSE_H
#define CLASSE_H
#include <QString>
#include <QObject>

#include "CQNPVUC.h"

class Classe : public QObject
{
    Q_OBJECT
private:
    Q_PROPERTY(QString nom READ nom WRITE setNom NOTIFY nomChanged)
    Q_PROPERTY(QString description READ description WRITE setDescription NOTIFY descriptionChanged)
    Q_PROPERTY(QString pathImage READ pathImage WRITE setPathImage NOTIFY pathImageChanged)


    map<Statistiques, int> stats;

    QString m_nom;

    QString m_description;

    QString m_pathImage;

public:
    Classe(QString nom, QString description, QString pathImage);

    QString nom() const;
    QString description() const;
    QString pathImage() const;

public slots:
    void setNom(QString arg);
    void setDescription(QString arg);
    void setPathImage(QString arg);

signals:
    void nomChanged(QString arg);
    void descriptionChanged(QString arg);
    void pathImageChanged(QString arg);
};

#endif // CLASSE_H
