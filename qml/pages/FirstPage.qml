import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page


    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All
    SilicaListView{
        anchors.fill: parent
        model:myModel
        header:PageHeader{
            title: "Personnages"
        }
        quickScroll:true
        delegate: ListItem{
            id:listItem
            contentHeight: 50


            Row{Label{text: prenom + " " + nom}

                Image{
                    id:image
                    fillMode: Image.PreserveAspectFit
                    source:"../../images/2.png"
                }

            }


            onClicked: {
                pageStack.push(detailPage,{perso: perso});
            }

            Component {
                id: detailPage
                SecondPage {}
            }
        }

        PullDownMenu{

            MenuItem{
                text:"supprimer Personnage"
                onClicked: {
                    Remorse.itemAction(myModel.columnCount(myModel),"supprimer",deleteLater(),true);
                }
            }

            MenuItem{
                text:"ajouter Personnage"
                onClicked: {
                    myModel.addGenericPersonnage();
                }
            }
        }
    }
}
