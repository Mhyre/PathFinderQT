import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    property var perso

    PageHeader {
        title: "Mon perso"
        id: header
    }

    Column {
        anchors.top: header.bottom
        width: parent.width

        TextField {
            text: perso.nom
            label: "Personnage"

            width: parent.width
            validator: RegExpValidator { regExp: /^[^/]*$/ }
            EnterKey.onClicked: {
                perso.nom = text
                sizeField.focus = true
            }
            onFocusChanged: text = Qt.binding( function() { return perso.nom} )
        }

        TextField {
            id: sizeFieldPrenom
            text: perso.prenom
            label: "Prenom :"
            validator: RegExpValidator { regExp: /\d{1,7}$/ }

            inputMethodHints: Qt.ImhDigitsOnly
            EnterKey.onClicked: {
                perso.prenom = text
                focus = false
            }
            onFocusChanged:  text = Qt.binding( function() { return perso.prenom} )
        }

        TextField {
            id: sizeFieldNom
            text: perso.nom
            label: "nom :"
            validator: RegExpValidator { regExp: /\d{1,7}$/ }

            inputMethodHints: Qt.ImhDigitsOnly
            EnterKey.onClicked: {
                perso.nom = text
                focus = false
            }
            onFocusChanged:  text = Qt.binding( function() { return perso.nom} )
        }

        TextField {
            id: sizeFieldSexe
            text: perso.sexe
            label: "sexe :"
            width: label.length*font.pointSize
            validator: RegExpValidator { regExp: /\d{1,7}$/ }

            inputMethodHints: Qt.ImhDigitsOnly
            EnterKey.onClicked: {
                perso.sexe = text
                focus = false
            }
            onFocusChanged:  text = Qt.binding( function() { return perso.sexe} )
        }

    }
}
